from django.urls import path

from .views import BooksList, BookDetail, CreateBook

urlpatterns = [
    path('books/', BooksList.as_view()),
    path('books/<int:id>', BookDetail.as_view()),
    path('create_book/', CreateBook.as_view())
]



