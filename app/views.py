from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from .models import Book, Author
from .serializers import BookSerializer


class BooksList(APIView):

    def get(self, request):
        books = Book.objects.all()
        serializer = BookSerializer(books, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class BookDetail(APIView):

    def get(self, request, id):
        book = Book.objects.get(pk=id)
        serializer = BookSerializer(book)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CreateBook(APIView):

    def post(self, request):
        serializer = BookSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)





