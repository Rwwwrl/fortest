from rest_framework import serializers

from .models import Book

class BookSerializer(serializers.ModelSerializer):

    author = serializers.CharField(source='author.id', read_only=True)

    class Meta:
        model = Book
        fields = ['title', 'author']


class CustomSerializer(serializers.Serializer):
    '''
    Кастомный сериализатор для проверок 
    '''
    name = serializers.CharField(max_length=100)    
    age = serializers.IntegerField()


    # кастомная проверка validate_ИмяПоля
    def validate_name(self, value):
        if len(value.split(' ')) > 1:
            raise serializers.ValidationError('Name must be one word string')
        return value