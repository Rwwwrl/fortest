from django.db import models


class Book(models.Model):

    title = models.CharField(max_length=255)
    date_of_creating = models.PositiveIntegerField()
    author = models.ForeignKey('Author', related_name='books', on_delete=models.CASCADE)

    def count_authors_of_this_book(self):
        count = 0
        for author in Author.objects.all():
            for book in author.books.all():
                if book.title == self.title:
                    count += 1
        return count

    def __str__(self):
        return self.title


class Author(models.Model):

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    age = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


